/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author armiixteryx
 */
public class Banco {
    ArrayList <Cuenta> cuentas;
    String entidadFinanciera;
    
    public Banco() {
        cuentas = new ArrayList<>();
        entidadFinanciera = "";
    }
    
    public void crearCuenta() {
        Scanner sc = new Scanner(System.in);
        Boolean entradaValida;
        int opcion;
        System.out.println("CREAR CUENTA");
        System.out.println("1. Cuenta vacia.");
        System.out.println("2. Cuenta con datos.");
        if(sc.hasNextInt()) {
            opcion = sc.nextInt();
            if(opcion == 1) {
                entradaValida = true;
                Cuenta obj = new Cuenta();
                cuentas.add(obj);
            } else if(opcion == 2) {
                Boolean band = false;
                String cuenta = "";
                String nombre = "";
                double saldo = 0;
                do {
                    System.out.println("Ingrese numero de cuenta: ");
                    if(sc.hasNextLong()) {
                        cuenta = sc.nextLine();
                        band = true;
                    }
                } while(band == false);
                System.out.println("Ingrese nombre del titular: ");
                nombre = sc.nextLine();
                
                band = false;
                do {
                    System.out.println("Ingrese el saldo: ");
                    if(sc.hasNextDouble()) {
                        saldo = sc.nextDouble();
                        band = true;
                    }
                } while(band == false);
                Cuenta obj = new Cuenta(cuenta, nombre, saldo);
            }
        }
        sc.close();
    }
    
    public void consultarCuenta() {
        Scanner sc = new Scanner(System.in);
        Iterator<Cuenta> it = cuentas.iterator();
        System.out.println("Ingrese numero de cuenta: ");
        
    }
    
    public int menu() {
        Scanner sc = new Scanner(System.in);
        int opcion = 0;
        Boolean entradaValida;
        do {
            System.out.println("MENU");
            System.out.println("1. Abrir cuenta.");
            System.out.println("2. Consultar estado de cuenta.");
            System.out.println("3. Realizar deposito.");
            System.out.println("4. Realizar retiro.");
            System.out.println("5. Salir.");
            System.out.println("Opcion: ");
            if(sc.hasNextInt()) {
                opcion = sc.nextInt();
                sc.close();
                entradaValida = true;
            } else {
                System.out.println("Entrada no valida. Intente nuevamente.");
                entradaValida = false;
            }
        } while(entradaValida == false);
        return opcion;
    }
    
    public void iniciar() {
        int opcion = menu();
        switch(opcion) {
            case 1:
                crearCuenta();
                break;
            case 2:
                break;
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
